package com.gitlab.avelyn.architecture.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

/**
 * The main building block of the engine.
 */
public class Component implements Toggleable {
    private final List<Runnable> enableListeners = new ArrayList<>();
    private final List<Runnable> disableListeners = new ArrayList<>();
    private boolean enabled = false;
    
    
    /** 
     * {@inheritDoc}
     * 
     * When {@link #isEnabled()} returns {@code false} this {@link Component}'s 
     * enable listeners({@link #getEnableListeners()}) are run in order.
     * After calling this method {@link #isEnabled()} will return {@code true}.
     *
     * @return - This {@link Component} for chaining.
     */
    @Override public Component enable() {
        if (!enabled) getEnableListeners().forEach(Runnable::run);
        enabled = true;
        return this;
    }
    
    /**
     * {@inheritDoc}
     * 
     * When {@link #isEnabled()} returns {@code true} this {@link Component}'s 
     * disable listeners({@link #getDisableListeners()}) are run in order.
     * After calling this method {@link #isEnabled()} will return {@code false}.
     *
     * @return - This {@link Component} for chaining.
     */
    @Override public Component disable() {
        if (enabled) getDisableListeners().forEach(Runnable::run);
        enabled = false;
        return this;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override public final boolean isEnabled() { return enabled; }
    
    /**
     * Returns the list of {@link Runnable}s to run when {@link #disable()} is called.
     *
     * @return - The {@link Runnable}s to run.
     */
    public final List<Runnable> getEnableListeners() { return enableListeners; }
    
    /**
     * Returns the list of {@link Runnable}s to run when {@link #disable()} is called.
     *
     * @return - The {@link Runnable}s to run.
     */
    public final List<Runnable> getDisableListeners() { return disableListeners; }
    
    
    /**
     * Adds {@link Runnable}s to this {@link Component}'s 
     * enable listeners({@link #getEnableListeners()}).
     *
     * Exactly equivalent to:
     * {@code getEnableListeners().addAll(Arrays.asList(listeners));}
     * 
     * @param listeners
     * 		- The {@link Runnable}s to be added.
     * @return - This {@link Component} for chaining.
     */
    public final Component onEnable(Runnable... listeners) {
        getEnableListeners().addAll(asList(listeners));
        return this;
    }
    
    /**
     * Adds {@link Runnable}s to this {@link Component}'s 
     * disable listeners({@link #getDisableListeners()}).
     * 
     * Exactly equivalent to:
     * {@code getDisableListeners().addAll(asList(listeners)); }
     *
     * @param listeners
     * 		- The {@link Runnable}s to be added.
     * @return - This {@link Component} for chaining.
     */
    public final Component onDisable(Runnable... listeners) {
        getDisableListeners().addAll(asList(listeners));
        return this;
    }
    
    /**
     * Adds a {@link Toggleable} that will be enabled({@link Toggleable#enable()})
     * and disabled({@link Toggleable#disable()}) with this {@link Component}.
     * 
     * Exactly equivalent to:
     * {@code onEnable(child::enable).onDisable(child::disable);
     * enabled ? child.enable() : child.disable();}
     * 
     * @param child
     * 		- The {@link Toggleable} to add.
     * @param <Child>
     * 		- The type of {@link Toggleable} to return.
     * @return - The {@link Toggleable} added.
     */
    public <Child extends Toggleable> Child child(Child child) { 
        onEnable(child::enable).onDisable(child::disable);
        return (Child) (enabled ? child.enable() : child.disable());
    }
    
    /**
     * Adds multiple {@link Toggleable}s that will be enabled({@link Toggleable#enable()})
     * and disabled({@link Toggleable#disable()}) with this {@link Component}.
     *
     * Exactly equivalent to:
     * {@code for (final Toggleable child : children) child(child);}
     * 
     * @param children
     *         - The {@link Toggleable[]} to add.
     * @return - The {@link Toggleable[]} added.
     */
    public Toggleable[] children(Toggleable... children) { 
        for (final Toggleable child : children) child(child);
        return children;
    }
    
    
    /**
     * @deprecated as of 1.0.8 because of verbosity, use {@link #child(Toggleable)}.
     * 
     * Adds a {@link Toggleable} that will be enabled({@link Toggleable#enable()})
     * and disabled({@link Toggleable#disable()}) with this {@link Component}.
     * 
     * @param child
     * 		- The {@link Toggleable} to add.
     * @param <Child>
     * 		- The type of {@link Toggleable} to return.
     * @return - The {@link Toggleable} added.
     */
    @Deprecated public <Child extends Toggleable> Child addChild(Child child) { return child(child); }
    
    /**
     * @deprecated as of 1.0.8 because of verbosity, use {@link #children(Toggleable...)}.
     * 
     * Adds multiple {@link Toggleable}s that will be enabled({@link Toggleable#enable()})
     * and disabled({@link Toggleable#disable()}) with this {@link Component}.
     *
     * Exactly equivalent to:
     * {@code for (final Toggleable child : children) child(child);}
     * 
     * @param children
     *         - The {@link Toggleable[]} to add.
     * @return - The {@link Toggleable[]} added.
     */
    @Deprecated public Toggleable[] addChild(Toggleable... children) { return children(children); }
}