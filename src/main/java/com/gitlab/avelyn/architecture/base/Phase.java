package com.gitlab.avelyn.architecture.base;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Phases are the second building block of the engine.
 * They are both {@link Completable} and {@link Toggleable}.
 */
public class Phase extends Component implements Completable { //TODO chaining returns
    private final List<Runnable> completeListeners = new ArrayList<>();
    private boolean complete = false;
    
    /**
     * {@inheritDoc}
     * 
     * When {@link #isEnabled()} returns {@code false} after calling
     * this method {@link #isComplete()} will return {@code false}.
     *
     * @return - This {@link Phase} for chaining.
     */
    @Override public Phase enable() {
        if (!isEnabled()) complete = false;
        return (Phase) super.enable();
    }
    
    /**
     * {@inheritDoc}
     * 
     * @return - This {@link Phase} for chaining.
     */
   @Override public Phase disable() { 
        return (Phase) super.disable();
    }
    
    /**
     * {@inheritDoc}
     * 
     * When {@link #isComplete()} returns {@code false} and {@link #isEnabled}
     * returns {@code true} this {@link Component}'s complete 
     * listeners({@link #getCompleteListeners()}) are run in order.
     * After calling this method {@link #isComplete()} will return {@code true}.
     *
     * @return - This {@link Phase} for chaining.
     */
   @Override public Phase complete() {
        if (!complete && isEnabled()) getCompleteListeners().forEach(Runnable::run);
        complete = true;
        return this;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override public final boolean isComplete() { return complete; }
    
    /**
     * Returns the list of {@link Runnable}s to run when {@link #complete()} is called.
     *
     * @return - The {@link Runnable}s to run.
     */
    public final List<Runnable> getCompleteListeners() { return completeListeners; }
    
    
    /**
     * Adds {@link Runnable}s to this {@link Phase}'s 
     * complete listeners({@link #getCompleteListeners()}).
     *
     * Exactly equivalent to:
     * {@code getCompleteListeners().addAll(Arrays.asList(listeners));}
     * 
     * @param listeners
     * 		- The {@link Runnable}s to be added.
     * @return - This {@link Phase} for chaining.
     */
    public final Phase onComplete(Runnable... listeners) {
        getCompleteListeners().addAll(asList(listeners));
        return this;
    }
    
    /**
     * Adds a {@link Runnable} that disables({@link #disable()} this {@link Phase} 
     * to it's complete listeners({@link #getCompleteListeners()}).
     * When {@link #isEnabled()} returns {@code false} after calling
     * this method {@link #isComplete()} will return {@code false}.
     * 
     * Exactly equivalent to:
     * {@code if (complete) disable(); onComplete(this::disable);}
     * 
     * @return - This {@link Phase} for chaining.
     */
    public Phase disableOnComplete() {
        if (complete) disable();
        return onComplete(this::disable);
    }
}